#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest

def python_built_in_sort(sequence):
    sequence.sort()

def selection_sort(sequence):
    """Sort supplied sequence in a naive way (selection sort).

  Implementation description: find maximum value and
  place it to the top, then next maximum and place it
  below the top and so on

  Sorting is done in place. Function always returns None.
    """
    u = len(sequence) - 1  # substract 1 here not to do so inside the loop
    while u > 0:
        i = 1
        max_value_index = 0
        while i <= u:
            if sequence[i] >= sequence[max_value_index]: # >= for stable sort
                max_value_index = i
            i += 1
        sequence[u], sequence[max_value_index] = sequence[max_value_index], sequence[u]
        u -= 1  # Reduce top point have everything above it already sorted

def selection_optimized_sort(sequence):
    """Sort supplied sequence in an optimized naive way (selection sort).

  Implementation description: find minimum and maximum value and
  place them to the bottom and to the top respectively, then find next
  minimum and maximum values and place them to the bottom and to the top
  respectively too and so on (move lower and upper boundary search for each
  iteration)

  Sorting is done in place. Function always returns None.
    """
    l = 0
    u = len(sequence) - 1  # substract 1 here not to do so inside the loop
    while l < u:
        max_value_index = l
        min_value_index = l
        i = l + 1
        while i <= u:
            if sequence[i] >= sequence[max_value_index]: # >= for stable sort
                max_value_index = i
            elif sequence[i] < sequence[min_value_index]: # < for stable sort
                min_value_index = i
            i += 1

        sequence[u], sequence[max_value_index] = sequence[max_value_index], sequence[u]
        if min_value_index == u: # correct min_value_index since we swapped elements
            min_value_index = max_value_index
        sequence[l], sequence[min_value_index] = sequence[min_value_index], sequence[l]
        u -= 1
        l += 1

def bubble_sort(sequence):
    """Bubble sort"""
    swapped = True
    while swapped:
        swapped = False
        for j in xrange(1, len(sequence)):
            if sequence[j - 1] > sequence[j]:
                sequence[j - 1], sequence[j] = sequence[j], sequence[j - 1]
                swapped = True

def bubble_optimized_sort(sequence):
    """Optimized bubble sort

       Optimization: reduce boundary of upper region in each iteration since above it everything is sorted
    """
    u = len(sequence)  # u = one more than max index
    swapped = True
    while swapped and u > 1:
        swapped = False
        for j in xrange(1, u):
            if sequence[j - 1] > sequence[j]:
                sequence[j - 1], sequence[j] = sequence[j], sequence[j - 1]
                swapped = True
        u -= 1

def bubble_even_more_optimized_sort(sequence):
    """Even More Optimized bubble sort

       Optimization: remember last swapped element, everything above it is sorted then
    """
    u = len(sequence) # u = always points to one more than max index
    swapped = True
    while swapped and u > 1:
        swapped = False
        for j in xrange(1, u):
            if sequence[j - 1] > sequence[j]:
                sequence[j - 1], sequence[j] = sequence[j], sequence[j - 1]
                swapped = True
                u = j + 1
        u -= 1

import random
import gc, time

class TestSortFunctions(unittest.TestCase):
    @staticmethod
    def createTestFunction(sort_function):
        """Create closure function from template that calls particular sort function"""
        def template_test_function(self):
            random_list_of_random_ints = [random.randint(-100, 100) for i in xrange(0, random.randint(1, 100))]
            model_list_of_random_ints = sorted(random_list_of_random_ints)
            sorted_list_of_random_ints = random_list_of_random_ints[:]
            sort_function(sorted_list_of_random_ints)
            print ""
            print 'Original sequence:        %s' % random_list_of_random_ints
            print 'Expected sorted sequence: %s' % model_list_of_random_ints
            print 'Actual sorted sequence:   %s' % sorted_list_of_random_ints
            self.assertEquals(sorted_list_of_random_ints, model_list_of_random_ints)

            gc.disable()
            iterations = 1000
            time_taken = 0
            for x in xrange(0, iterations):
                t_start = time.time()
                sort_function([random.randint(-100, 100) for i in xrange(0, 100)])
                time_taken += time.time() - t_start
            gc.enable()
            print 'Time taken for %s iterations is %s seconds' % (iterations, time_taken)

        return template_test_function

from types import MethodType

if __name__ == '__main__':
    for k, v in globals().items():
        if k.endswith('_sort') and callable(v):
            setattr(TestSortFunctions, 'test_%s' % k, MethodType(TestSortFunctions.createTestFunction(v), None, TestSortFunctions))

    suite = unittest.TestLoader().loadTestsFromTestCase(TestSortFunctions)
    unittest.TextTestRunner(verbosity=3).run(suite)